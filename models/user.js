//Activity 1:


const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		//type of data
		type: String,
		required: [true, "First name is required"]

	},
		lastName: {
		//type of data
		type: String,
		required: [true, "Last name is required"]

	},
		email: {
		//type of data
		type: String,
		required: [true, "Email is required"]

	},
	password: {
		//type of data
		type: String,
		required: [true, "Password is required"]

	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	mobileNo : {
		type: String,
		required: [true, "Mobile number is required"]
	},
	enrollments : [
		{
			courseId : {
				type: String,
				required: [true, "ID is required"]
			},
			enrolledOn : {
				type: Date,
				default: new Date()
			},
			status: {

				type: String,
				// for now the it is enrolled since there is not payment method yet, but if there is, it should be pending
				default: "Enrolled"
			}
		}


	]




})

//if you want to add a PHP in case there is a payment use this, php: Number required: [true, "payment is required"]

// user should be singular since in atlas it will be converted to plural
module.exports = mongoose.model("User", userSchema);