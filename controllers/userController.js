const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the find method returns a record if a match is found

		if (result.length > 1) {

			return {
				message: " Duplicate User"
			} 

		} else if (result.length > 0){

			return {
				message: "User was found"
			}

		} else {

			return {
				message: "User not found"
			}
		}

		
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({

		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if (error) {

			return false;
		} else {

			return user
		};
	});
};


//User auth: 

/*
	steps: 
	1. check the database if the user email exists
	2. compare password provided in the login form with the password stored in the database
	3. generate/return a JSON webtoken if the user succesfully logged in and return false if not

*/

module.exports.loginUser = (reqBody) => {
		return User.findOne({email : reqBody.email}).then(result => {

			if (result == null ) {

				return {
					message : "Not found in our DB"
				}

			} else {

				// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result

				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if (isPasswordCorrect) {

					return {access : auth.createAccessToken(result)}

				} else {

					//if password doesnt match

					return {
						message : "Password was incorrect"
					}
				};
			};

		});
};


//=+++++++++++++

//activity 2

//module.exports.getProfile = (data ) => { }