const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController")

//Check email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(result => res.send(result));

});


//Route for reg

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(result => res.send(result))
});

//Route for user authentication

router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(result => res.send(result))
})



module.exports = router;

//Activity 2

/*module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};*/