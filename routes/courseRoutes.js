const express = require("express");
const router = express.Router();

const auth = require("../auth")

const coureController = require("../controllers/courseController");


//Router for creating a course

router.post("/addCourse", auth.verify, (req, res) => {

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	coureController.addCourse(data).then(result => res.send (result));
});



module.exports = router;


// [ACTIVITY]

// route function that retrieving all the courses